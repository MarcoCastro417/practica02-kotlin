package com.example.practica02_kotlin

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {

    private lateinit var txtAltura: EditText
    private lateinit var txtPeso: EditText
    private lateinit var txtImc: TextView
    private lateinit var lblRecomendacion: TextView
    private lateinit var idCalcular: Button
    private lateinit var idLimpiar: Button
    private lateinit var idCerrar: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        txtAltura = findViewById(R.id.txtAltura)
        txtPeso = findViewById(R.id.txtPeso)
        txtImc = findViewById(R.id.txtImc)
        lblRecomendacion = findViewById(R.id.lblRecomendacion)
        idCalcular = findViewById(R.id.idCalcular)
        idLimpiar = findViewById(R.id.idLimpiar)
        idCerrar = findViewById(R.id.idCerrar)

        idCalcular.setOnClickListener {
            if ((txtAltura.text.isNullOrEmpty()) || (txtPeso.text.isNullOrEmpty())) {
                Toast.makeText(this, "Faltó capturar información", Toast.LENGTH_SHORT).show()
            } else {
                calcularImc()
            }
        }

        idLimpiar.setOnClickListener {
            limpiarCampos()
        }

        idCerrar.setOnClickListener {
            finish()
        }
    }

    private fun calcularImc() {
        val altura = txtAltura.text.toString().toDouble() / 100
        val peso = txtPeso.text.toString().toDouble()
        val imc = peso / (altura * altura)

        val formato = DecimalFormat("#.#")
        val resultado = formato.format(imc)

        txtImc.text = resultado

        val resultadoRec = resultado.toDouble()
        when {
            resultadoRec < 18.5 -> lblRecomendacion.setText("Bajo Peso")
            resultadoRec < 24.9 -> lblRecomendacion.setText("Peso saludable")
            resultadoRec < 29.9 -> lblRecomendacion.setText("Sobrepeso")
            else -> lblRecomendacion.setText("Obesidad")
        }
    }

    private fun limpiarCampos() {
        txtAltura.setText("")
        txtPeso.setText("")
        txtImc.text = "Su IMC es: "
    }
}